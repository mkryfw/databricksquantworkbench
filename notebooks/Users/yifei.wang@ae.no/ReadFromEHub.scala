// Databricks notebook source
import org.apache.spark.eventhubs._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._

    // Build connection string with the above information
    val connectionString = ConnectionStringBuilder("Endpoint=sb://ae-sharedeventhub-ref.servicebus.windows.net/;SharedAccessKeyName=test;SharedAccessKey=oYNpiHGUtnaZkhJAwxARbBpO7f6LX9OoUjA6lW548qE=")
      .setEventHubName("spontanwerte-ref")
      .build

    val customEventhubParameters =
      EventHubsConf(connectionString)
      .setMaxEventsPerTrigger(5)

    val incomingStream = spark.readStream.format("eventhubs").options(customEventhubParameters.toMap).load()

    //incomingStream.printSchema


    // Event Hub message format is JSON and contains "body" field
    // Body is binary, so we cast it to string to see the actual content of the message
    val messages =
      incomingStream
      .withColumn("Offset", $"offset".cast(LongType))
      .withColumn("Time (readable)", $"enqueuedTime".cast(TimestampType))
      .withColumn("Timestamp", $"enqueuedTime".cast(LongType))
      .withColumn("Body", $"body".cast(StringType))
      .select("Offset", "Time (readable)", "Timestamp", "Body")


    messages.createOrReplaceTempView("spontanwerte")


    //messages.printSchema
    //messages.writeStream.outputMode("append").format("console").option("truncate", false).start().awaitTermination()


// COMMAND ----------



var streamingSelectDF = 
  messages
   .select(get_json_object(($"body").cast("string"), "$.spontanWerteHeader").alias("spontanWerteHeader"))

display(streamingSelectDF)




// COMMAND ----------



dbutils.fs.mount(
  source = "adl://<your-data-lake-store-account-name>.azuredatalakestore.net/<your-directory-name>",
  mountPoint = "/mnt/<mount-name>",
  extraConfigs = configs)





// COMMAND ----------

// MAGIC %sql
// MAGIC select Body from spontanwerte

// COMMAND ----------

// MAGIC %sql
// MAGIC 
// MAGIC SELECT *
// MAGIC 
// MAGIC FROM spontanwerte
// MAGIC   LATERAL VIEW json_tuple(Body.jsonObject, 'spontanWerteHeader', 'spontanWerteMsg') V1
// MAGIC      as werteheader, wertemsg
// MAGIC   LATERAL VIEW json_tuple(V1.werteheader, 'msgId', 'sendDatetime') V2
// MAGIC     as msgId, senddatetime
// MAGIC   LATERAL VIEW json_tuple(V2.wertemsg, 'meterpoint', 'power', 'average', 'wind', 'eisman') V3
// MAGIC     as meterpoint, power, average, wind, eisman

// COMMAND ----------

// MAGIC %python
// MAGIC 
// MAGIC resultsDF = spark.sql("SELECT Body FROM spontanwerte")
// MAGIC 
// MAGIC resultsDF.head()

// COMMAND ----------



// COMMAND ----------

// MAGIC %python
// MAGIC 
// MAGIC 
// MAGIC spontanwete_df = (spark.readStream
// MAGIC     .format("eventhubs")
// MAGIC     .option("connectionstring", LOG_HOST)
// MAGIC     .option("port", LOG_PORT)
// MAGIC     .load()
// MAGIC )
// MAGIC 
// MAGIC 
// MAGIC     // Build connection string with the above information
// MAGIC     val connectionString = ConnectionStringBuilder("Endpoint=sb://ae-sharedeventhub-ref.servicebus.windows.net/;SharedAccessKeyName=test;SharedAccessKey=oYNpiHGUtnaZkhJAwxARbBpO7f6LX9OoUjA6lW548qE=")
// MAGIC       .setEventHubName("spontanwerte-ref")
// MAGIC       .build
// MAGIC 
// MAGIC     val customEventhubParameters =
// MAGIC       EventHubsConf(connectionString)
// MAGIC       .setMaxEventsPerTrigger(5)
// MAGIC       
// MAGIC format("eventhubs").options(customEventhubParameters.toMap).load()

// COMMAND ----------

// MAGIC %python
// MAGIC 
// MAGIC from pyspark.sql.functions import *
// MAGIC 
// MAGIC running_counts_df = messages.agg(count("*"))
// MAGIC display(running_counts_df)